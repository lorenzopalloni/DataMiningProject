###
#  Created by lore on 14/02/19.
###

import pandas as pd


class Encoder:
    """
    Encoder have two main features:
        * Numeric encoding
            can transform the unique values of all 'object' type (or 'categorical') variables of a pandas.DataFrame into
            numbers of type 'int64' keeping a lexicographic order.
        * OneHot encoding

    Example:

        encoder = Encoder()
        data_encoded = encoder.fit_encode(data, drop=True)  # Categorical variables replaced by encoded version.
        data = encoder.decode(drop=True)  # Categorical variables decoded and encoded columns are dropped.

    Another example:

        encoder = Encoder()
        encoder.fit(data)

        series_encoded = encoder.encode_series(data[var_name])  # Coding of a single pandas.Series done.
        series_decoded = encoder.decode_series(series_encoded)  # Decoding of the single pandas.Series done.

    """

    def __init__(self):
        self.data = None
        self.encoder_dict = None
        self.obj_names = None

    @staticmethod
    def make_code_dict_series(series):
        unique_values = series.unique().tolist()

        try:
            unique_values.sort()
        except TypeError:
            list(map(str, series)).sort()
            unique_values = series.unique().tolist()
            unique_values.sort()
            print("all the values of the 'object' series are casted to 'str' in order to sort them.")

        code_dict = {key: num for num, key in enumerate(unique_values)}
        return code_dict

    def fit(self, data, obj_names=None):
        self.data: pd.DataFrame = data
        self.obj_names = obj_names if obj_names else data.select_dtypes('object').columns.tolist()
        self.encoder_dict = {key: value for key, value in zip(self.obj_names, [self.make_code_dict_series(self.data[name]) for name in self.obj_names])}

    def encode(self, drop=None):
        assert self.encoder_dict is not None,\
            "You have to call the Encoder.fit method before the Encoder.encode method"

        for name in self.obj_names:
            self.data["{0}_encoded".format(name)] = self.encode_series(self.data[name])
        if drop is not None:
            self.data.drop(columns=self.obj_names, inplace=True)
        return self.data

    def fit_encode(self, data, obj_list=None, drop=None):
        self.fit(data, obj_list)
        return self.encode(drop)

    def encode_series(self, series_to_encode):
        assert self.encoder_dict is not None,\
            "You have to call the Encoder.fit method before the Encoder.encode_series method"
        series_encoded = pd.Series([0] * series_to_encode.shape[0])
        idx = 0
        for key in series_to_encode:
            series_encoded[idx] = self.encoder_dict[series_to_encode.name][key]
            idx += 1
        series_encoded.index = series_to_encode.index
        series_encoded.name = series_to_encode.name + '_encoded'
        return series_encoded

    def decode(self, drop=None):
        for name in self.obj_names:
            self.data[name] = self.decode_series(self.data[name + '_encoded'])
        if drop is not None:
            self.data.drop(columns=list(map(lambda x: x.__add__('_encoded'), self.obj_names)), inplace=True)
        return self.data

    def decode_series(self, series_to_decode):
        name = series_to_decode.name
        name_of_series_to_decode = name[:-8] if name[-8:] == '_encoded' else name
        series_decoded = pd.Series([' '] * series_to_decode.shape[0])
        idx = 0
        inv_code_dict = {value: str(key) for key, value in self.encoder_dict[name_of_series_to_decode].items()}
        for value in series_to_decode:
            series_decoded[idx] = inv_code_dict[value]
            idx += 1
        series_decoded.index = series_to_decode.index
        series_decoded.name = name_of_series_to_decode
        return series_decoded

    def fit_one_hot_encode(self, data, obj_names=None, drop=None):
        self.fit(data, obj_names)
        return self.one_hot_encode(drop)

    def one_hot_encode(self, drop=True):
        assert self.encoder_dict is not None,\
            "You have to call the Encoder.fit method before the Encoder.encode method"
        for name in self.obj_names:
            self.data = self.data.join(self.one_hot_encode_series(self.data[name]))
        if drop is not None:
            self.data.drop(columns=self.obj_names, inplace=True)
        return self.data

    def one_hot_encode_series(self, series):
        assert self.encoder_dict is not None,\
            "You have to call the Encoder.fit method before the Encoder.one_hot_encode_series method"

        ohe = {x: pd.Series([0] * series.shape[0]) for x in self.encoder_dict[series.name].keys()}
        idx = 0
        for key in series:
            ohe[key][idx] = 1
            idx += 1
        return pd.DataFrame(ohe)


def test_encoder(n_test=0):
    data = pd.read_csv(INTERIM / "preEncoding.csv")
    encoder = Encoder()
    if n_test == 0:
        print("data.head() BEFORE: \n", data.head())
        encoder.fit(data)
        data = encoder.encode()
        print("AFTER Encoder.fit(data) >> Encoder.encode(): \n", data.head())
        data.head()
    if n_test == 1:
        print("data.head() BEFORE: \n", data.head())
        encoder.fit(data)
        data = encoder.one_hot_encode()
        print("AFTER Encoder.fit(data) >> Encoder.one_hot_encode(): \n", data.head())
    if n_test == 2:
        encoder.fit(data)
        var_name = 'Scuola_provenienza'
        series_encoded = encoder.encode_series(data[var_name])  # Coding of a single pandas.Series done.
        print("First 5 values of {0}_encoded:\n".format(var_name), series_encoded.head())
        series_decoded = encoder.decode_series(series_encoded)  # Decoding of the single pandas.Series done.
        print(series_decoded.head())
    if n_test == 3:
        print(data.head())
        data = encoder.fit_encode(data, drop=True)
        print(data.head())


if __name__ == "__main__":
    from src.constants.paths import INTERIM

    test_encoder(1)
