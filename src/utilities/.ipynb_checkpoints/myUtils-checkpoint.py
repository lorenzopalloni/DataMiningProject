import numpy as np
import pandas as pd
from path import Path
import matplotlib.pyplot as plt
import seaborn as sns


def bar_plot(series, save_path=None, title=None):
    fig, ax = plt.subplots(figsize=(15, 8))
    sns.barplot(x=series.value_counts().index.tolist(),
                y=series.value_counts(),
                ax=ax)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    ax.yaxis.label.set_text('counts')
    ax.yaxis.label.set_fontsize(14)
    if title is not None:
        ax.title.set_text(title)
    else:
        ax.title.set_text(series.name)
    ax.title.set_fontsize(16)
    ax.title.set_fontweight('bold')
    if save_path is not None:
        plt.savefig(str(save_path), format='png')


def hist_plot(series, save_path=None, title=None):
    fig, ax = plt.subplots(figsize=(15, 8))
    sns.distplot(a=pd.Series(series.dropna(axis=0)), ax=ax)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    ax.xaxis.label.set_text("'{0}' - Values".format(series.name))
    ax.xaxis.label.set_fontsize(14)
    if title is not None:
        ax.title.set_text(title)
    else:
        ax.title.set_text(series.name)
    ax.title.set_fontsize(16)
    ax.title.set_fontweight('bold')
    if save_path is not None:
        plt.savefig(str(save_path), format='png')

        
def mse(y_true, y_pred):
    assert len(y_true) == len(y_pred), "The two provided vector must have the same length."
    return np.sum(np.power(y_true.__add__(-y_pred), 2)) / len(y_true)


def min_value_idx(array):
    min_value = array[0]
    min_idx = 0
    for i in range(len(array)):
        if array[i] < min_value:
            min_value = array[i]
            min_idx = i
    return min_idx


def highlight_esame_matematica_outliers(s, threshold=3):
    is_out = s < threshold
    return ['background-color: yellow' if v else '' for v in is_out]


def highlight_crediti_totali_outliers(s):
    '''
    This function is awful but i couldn't do better :C
    But given its utility i believe it is justified :D
    '''
    is_in = (70 < s) == (s < 120)
    return ['background-color: yellow' if v else '' for v in is_in]
